<h1 align="center">太空人</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

MicroPython 驱动 oled 屏幕显示动画，前往 [B 站观看视频](https://www.bilibili.com/video/BV1AL411L7yu/)

<p align="center"><img src="https://gitee.com/walkline/spaceman/raw/master/images/spaceman.gif" /></p>

### 动画播放原理

播放动画是最简单的，就是循环打开图片文件并显示在屏幕上

图片文件格式选择的是`.PBM`，之前使用`.BMP`格式的位图偶尔会出现显示问题，究其原因是因为位图数据有长度要求，长度不足的会自动补零导致花屏，因此不得不放弃

### 如何生成图片文件

第一步肯定是要下载你想要播放的`.gif`文件，并**保存到`images/`目录下**，同时，在`images/`目录下已经提供了几张动图可供测试

#### 自动方式

```bash
$ cd path/to/repo
$ python3 images/auto_convert.py
```

如果程序正确运行，会在`images/pbm/`下生成一系列分解图片

> 使用自动方式也许会生成许多重复的图片，代码中已经提供了基于`MD5`的去重函数，但是可能会导致动画不连贯，所以已经禁用了去重功能

#### 手动方式

如果自动方式生成的图片数量较大且重复内容居多，可以考虑使用手动方式生成分解图片

1. 根据屏幕尺寸（最短边）使用 [Gif调整大小工具](https://resizegif.imageonline.co/index-cn.php) 按比例缩放动图文件

2. 使用 [动画GIF到JPG工具](https://giftojpg.imageonline.co/index-cn.php) 生成动图文件的分解图片（`.jpg`），并按照播放顺序重命名所有文件

3. 手动删除多余的图片文件

4. 将所有`.jpg`文件移动到`images/jpg/`目录下

5. 将`.jpg`转换为`.pbm`

	```bash
	$ cd path/to/repo
	$ python3 images/convert.py
	```

### 如何播放动画

使用`ab 工具`上传所需文件

```bash
$ cd path/to/repo
$ ab
```

运行`main.py`文件

```bash
$ ab --repl
# ctrl + r，选择 main.py 并回车
```

### 版权声明

* 太空人动图来自 [微博](https://weibo.com/2236185177/K7fR8lCQO?refer_flag=1001030103_)

* 其它动图来自网络和 QQ 群

### 附录

`ab 工具`安装及使用说明请访问 [AMPY Batch Tool](https://gitee.com/walkline/a-batch-tool) 查看

### 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
	* 走线物联：[163271910](https://jq.qq.com/?_wv=1027&k=xtPoHgwL)
	* 扇贝物联：[31324057](https://jq.qq.com/?_wv=1027&k=yp4FrpWh)

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
